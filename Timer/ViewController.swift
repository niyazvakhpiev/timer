import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var HoursLabel: UILabel!
    @IBOutlet weak var MinutesLabel: UILabel!
    @IBOutlet weak var CancelButton: UIButton!
    @IBOutlet weak var StartButton: UIButton!
   
    var timer: Timer? = nil
    var totalsecunds = 300
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CancelButton.layer.cornerRadius = 5.0
        StartButton.layer.cornerRadius = 5.0
    }
    @IBAction func HandleCancelClick (_ sender: UIButton){
        HoursLabel.text = "05"
        MinutesLabel.text = "00"
        totalsecunds = 300
        timer?.invalidate()
    }
    @IBAction func HandleStartClick (_ sender: UIButton ){
        HoursLabel.text = "05"
        MinutesLabel.text = "00"
        timer?.invalidate()
        totalsecunds = 300
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Update), userInfo: nil, repeats: true)
    }
    @objc func Update (){
        if totalsecunds == 0{
            timer?.invalidate()
        }
        else {
            totalsecunds = totalsecunds - 1
            HoursLabel.text = "\(totalsecunds / 60)"
            MinutesLabel.text = "\(totalsecunds % 60)"
        }
    }
}

